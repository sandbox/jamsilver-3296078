# Config Sort

This module sorts all configuration objects recursively by key on export.
The idea is to produce more stable yaml files and reduce the frequency of
merge conflicts.

It depends on Config filter module.
