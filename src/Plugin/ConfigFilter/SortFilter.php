<?php

namespace Drupal\config_sort\Plugin\ConfigFilter;

use Drupal\config_filter\Plugin\ConfigFilterBase;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Config filter for sorting configuration.
 *
 * @ConfigFilter(
 *   id = "config_sort",
 *   label = @Translation("Config Sort"),
 *   storages = {"config.storage.sync"},
 * )
 */
class SortFilter extends ConfigFilterBase implements ContainerFactoryPluginInterface {

  use DependencySerializationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function filterWrite($name, array $data) {
    static::recursiveKsort($data);
    return $data;
  }

  /**
   * Recursively sorts all associative portions of an array by key.
   *
   * Any numeric arrays (or sub-arrays) are left in source order.
   *
   * @param array $array
   *   An array to sort.
   */
  protected static function recursiveKsort(array &$array) {
    $hasStringKey = FALSE;
    foreach (array_keys($array) as $key) {
      if (is_array($array[$key])) {
        static::recursiveKsort($array[$key]);
      }
      if (is_string($key)) {
        $hasStringKey = TRUE;
      }
    }
    if ($hasStringKey) {
      ksort($array);
    }
  }

}
